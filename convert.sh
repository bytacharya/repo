#!/bin/sh
allcurtains='./*.curtain'
for filename in $allcurtains
do
echo ${filename}
sleep 10


#set POSIX variables
sed -i 's/$name/%name%/g' ${filename}
sed -i 's/${name}/%name%/g' ${filename}
sed -i 's/$source/%source%/g' ${filename}
sed -i 's/$ver/%ver%/g' ${filename}
sed -i 's/${source}/%source%/g' ${filename}
sed -i 's/${ver}/%ver%/g' ${filename}
#Insert " ," to build, depends and conflicts
sed -i '/^build=/s/ /, /g' ${filename}
sleep 1
sed -i '/^depends=/s/ /, /g' ${filename}
sed -i '/^conflicts=/s/ /, /g' ${filename}
#Add package to top
sed -i '1 i [package]' ${filename}
#Replace examp=le with examp = "le"
sed -i '0,/name=*/{s/name=*/name = "/}' ${filename}
awk -i inplace '/^name = / {$0=$0"\""} 1'  ${filename}
sed -i '0,/ver=*/{s/ver=*/ver = "/}' ${filename}
awk -i inplace '/^ver = / {$0=$0"\""} 1'  ${filename}
sed -i '0,/des="/{s/des="/des = "/}' ${filename}
sed -i '0,/source=/{s/source=/source = "/}' ${filename}
#make dependencies toml compatible
sed -i 's/build=(/build = [ /g' ${filename}
sed -i '/^build =/s/.$/]/' ${filename}
sed -i 's/depends=(/depends = [ /g' ${filename}
sed -i '/^depends =/s/.$/]/' ${filename}
awk -i inplace '/^source =/ {$0=$0"\""} 1'  ${filename}
sed -i 's/conflicts=(/conflicts = [ /g' ${filename}
sed -i '/^conflicts =/s/.$/]/' ${filename}
#now turning the install section into toml formating
sed -i 's/prepare() {/prepare = \"\"\"/' ${filename}
sed -i 's/installation() {/installation = \"\"\"/' ${filename}
sed -i 's/cleaning() {/cleaning = \"\"\"/' ${filename}

sed -i '$ s/.$//' ${filename}
echo '"""' >> ${filename}

#Remove the last } and replace them with """
perl -0777 -pi -e 's/(.*)}(.*?)/\1\2/s' ${filename}
perl -0777 -pi -e 's/(.*)}(.*?)/\1\2/s' ${filename}
sed -i 's/}//' ${filename}

#finish and print to new file
 cat ${filename}   |tr '\n' '?'|sed -e 's/cleaning/"""?cleaning/'  |sed -e 's/installation/"""?installatiion/'|tr '\?' '\n' > test.toml
cat test.toml > ${filename}
done
